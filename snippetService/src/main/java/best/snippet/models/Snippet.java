package best.snippet.models;

import java.util.List;
import java.util.Objects;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Snippet {

	@Id
	private ObjectId id;
	
	private String content;
	private List<Tag> tags;
	private int likes;
	private User author;
	private List<Comment> comments;
	
	public Snippet() {
		super();
	}

	public Snippet(ObjectId id) {
		super();
		this.id = id;
	}

	public String getId() {
		return id.toHexString();
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public List<Tag> getTags() {
		return tags;
	}

	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}

	public int getLikes() {
		return likes;
	}

	public void setLikes(int likes) {
		this.likes = likes;
	}

	public User getAuthor() {
		return author;
	}

	public void setAuthor(User author) {
		this.author = author;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	@Override
	public int hashCode() {
		return Objects.hash(author, comments, content, id, likes, tags);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Snippet)) {
			return false;
		}
		Snippet other = (Snippet) obj;
		return Objects.equals(author, other.author) && Objects.equals(comments, other.comments)
				&& Objects.equals(content, other.content) && Objects.equals(id, other.id) && likes == other.likes
				&& Objects.equals(tags, other.tags);
	}

	@Override
	public String toString() {
		return "Snippet [id=" + id.toHexString() + ", content=" + content + ", tags=" + tags + ", likes=" + likes + ", author="
				+ author + ", comments=" + comments + "]";
	}
}
