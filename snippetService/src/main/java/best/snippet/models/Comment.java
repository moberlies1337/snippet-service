package best.snippet.models;

import java.util.Objects;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Comment {

	@Id
	private ObjectId id;
	private String content;
	private User author;

	public Comment() {
		super();
	}

	public Comment(ObjectId id) {
		super();
		this.id = id;
	}

	public Comment(ObjectId id, String content, User author) {
		super();
		this.id = id;
		this.content = content;
		this.author = author;
	}

	public String getId() {
		return id.toHexString();
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public User getAuthor() {
		return author;
	}

	public void setAuthor(User author) {
		this.author = author;
	}

	@Override
	public int hashCode() {
		return Objects.hash(author, content, id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Comment)) {
			return false;
		}
		Comment other = (Comment) obj;
		return Objects.equals(author, other.author) && Objects.equals(content, other.content)
				&& Objects.equals(id, other.id);
	}

	@Override
	public String toString() {
		return "Comment [id=" + id.toHexString() + ", content=" + content + ", author=" + author + "]";
	}
}
