package best.snippet.repositories;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;

import best.snippet.models.Snippet;
import best.snippet.models.Tag;


public interface SnippetRepository extends MongoRepository<Snippet, ObjectId> {

	List<Snippet> findByTags(@Param("tags") List<Tag> tags);
}
