package best.snippet.controllers;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import best.snippet.models.Snippet;
import best.snippet.services.SnippetService;

@CrossOrigin("*")
@RestController
@RequestMapping("/snippets")
public class SnippetController {
	
	@Autowired
	private SnippetService service;

	@PostMapping
	public Snippet create(@RequestBody Snippet s) {
		return service.create(s);
	}
	
	@GetMapping
	public List<Snippet> findAll() {
		service.findAll().forEach(System.out::println);
		return service.findAll();
	}

	@GetMapping("/{id}")
	public Snippet find(@PathVariable("id") ObjectId id) {
		return service.findById(id);
	}
	
	@PatchMapping
	public Snippet update(@RequestBody Snippet s) {
		return service.update(s);
	}
	
	@DeleteMapping("/{id}")
	public void delete(@PathVariable("id") ObjectId id) {
		service.delete(id);
	}
	
//	@DeleteMapping
//	public void deleteAll() {
//		service.deleteAll();
//	}
}
