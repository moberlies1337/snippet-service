package best.snippet.services;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import best.snippet.models.Snippet;
import best.snippet.repositories.SnippetRepository;

@Service
public class SnippetService {
	
	@Autowired
	private SnippetRepository repository;
	
	public Snippet create(Snippet s) {
		return repository.save(s);
	}
	
	public List<Snippet> findAll() {
		return repository.findAll();
	}

	public Snippet findById(ObjectId id) {
		return repository.findById(id).orElse(null);
	}
	
	public Snippet update(Snippet s) {
		return repository.save(s);
	}
	
	public void delete(ObjectId id) {
		repository.deleteById(id);
	}
	
	public void deleteAll() {
		repository.deleteAll();
	}
}
